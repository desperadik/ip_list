Django==2.2
django-extensions==2.1.6
django-widget-tweaks==1.4.3
pytz==2019.1
six==1.12.0
sqlparse==0.3.0
