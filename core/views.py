from django.contrib.auth.views import LoginView
from django.urls import reverse


class CustomLoginView(LoginView):

    """ Страница логина без авторедиректа"""

    template_name = 'core/login_page.html'

    def get_success_url(self):

        if self.request.user.is_authenticated:
            if self.request.user.is_superuser:
                return reverse('subnet_admin:ip_network:list')
            else:
                return reverse('subnet_user:ip_network:list')
