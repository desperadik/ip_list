import ipaddress

from django.db import models
from django_extensions.db.models import TimeStampedModel
from django.contrib.auth import get_user_model

from core.ip_methods import get_free_ip_from_subnet, get_ip_list_for_subnet

User = get_user_model()


class Subnet(TimeStampedModel):

    """ Модель описывает подсеть"""

    # (Subnet mask CIDR by RFC 1878 https://tools.ietf.org/html/rfc1878)
    SUBNET_MASK = (
        ('30', "255.255.255.252"),
        ('29', "255.255.255.248"),
        ('28', "255.255.255.240"),
        ('27', "255.255.255.224"),
        ('26', "255.255.255.192"),
        ('25', "255.255.255.128"),
        ('24', "255.255.255.0"),
        ('23', "255.255.254.0"),
        ('22', "255.255.252.0"),
        ('21', "255.255.248.0"),
        ('20', "255.255.240.0"),
        ('19', "255.255.224.0"),
        ('18', "255.255.192.0"),
        ('17', "255.255.128.0"),
        ('16', "255.255.0.0"),
    )

    ip_net = models.GenericIPAddressField(protocol='both')
    mask = models.CharField(choices=SUBNET_MASK, max_length=2, default='24')
    gateway = models.GenericIPAddressField(protocol='both')

    class Meta:
        ordering = ['-created']
        unique_together = ('ip_net', 'mask')

    def __str__(self):
        return '{0}/{1}'.format(self.ip_net, self.mask)

    def get_ip_list(self):

        """ Возвращает `list` всех достпных хостов для подсети"""

        return get_ip_list_for_subnet(self.ip_net, self.mask)

    def get_ip(self):

        """ Возвращает первый свободный IP `str` для данной подсети"""

        return get_free_ip_from_subnet(self.ip_net, self.mask, self.get_used_ip())

    def get_used_ip(self):

        """ Возвращает `list` список выданных IP для данной подсети
            Примечание: Адрес шлюза, тоже считается используемым.
        """
        excluded_ip_list = IPDelivery.used_ip_in_subnet(self)

        excluded_ip_list.append(self.gateway)

        return excluded_ip_list

    def get_used_ip_count(self):

        """ Возвращает `int` количество выданных IP адресов для подсети """

        return len(self.get_used_ip())

    def get_ip_count(self):

        """ Возвращает `int` количество IP адресов для подсети """

        return len(self.get_ip_list())

    def get_allowed_ip(self):

        """ Возвращает `int` количество свободных IP адресов для подсети """

        return self.get_ip_count() - self.get_used_ip_count()


class IPDelivery(TimeStampedModel):

    """ Модель описывает выданные IP адреса с привязкой к `Subnet` и `User` """

    user = models.ForeignKey(User, on_delete='CASCADE')
    delivery_ip = models.GenericIPAddressField(protocol='both')
    subnet = models.ForeignKey(Subnet, on_delete='CASCADE')

    class Meta:
        ordering = ['-created']
        unique_together = ('delivery_ip', 'subnet')

    def __str__(self):
        return self.delivery_ip

    @classmethod
    def used_ip_in_subnet(cls, subnet):

        """ Возвращает `list` список выданных IP адресов для подсети"""

        ip_list = [item.delivery_ip for item in cls.objects.filter(subnet=subnet)]
        return ip_list
