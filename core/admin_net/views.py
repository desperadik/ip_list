from django.contrib import messages
from django.urls import reverse
from django.views.generic import ListView, DeleteView, CreateView, UpdateView

from core.forms import IPNetworkForm, FilterForm
from core.models import Subnet, IPDelivery
from core.mixins import DecoratorChainingMixin
from core.decorators import admin_and_login_required


class SubnetList(DecoratorChainingMixin, ListView):

    """ Список подсетей """

    decorators = [admin_and_login_required, ]
    model = Subnet
    template_name = 'core/admin_net/subnet_list.html'
    context_object_name = 'subnet_list'
    paginate_by = 10


class SubnetAdd(DecoratorChainingMixin, CreateView):

    """ Рендер формы создания подсети """

    decorators = [admin_and_login_required, ]
    model = Subnet
    form_class = IPNetworkForm
    template_name = 'core/admin_net/subnet_form.html'
    success_url = 'subnet_admin:ip_network:list'
    success_text = 'Подсеть успешно добавлена.'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, self.success_text)
        return reverse(self.success_url)


class SubnetEdit(DecoratorChainingMixin, UpdateView):

    """ Рендер формы редактирования подсети """

    decorators = [admin_and_login_required, ]
    model = Subnet
    form_class = IPNetworkForm
    template_name = 'core/admin_net/subnet_form.html'
    success_url = 'subnet_admin:ip_network:list'
    success_text = 'Подсеть успешно сохранена.'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, self.success_text)
        return reverse(self.success_url)


class SubnetDelete(DecoratorChainingMixin, DeleteView):

    """ Рендер формы подтверждения удаления подсети """

    decorators = [admin_and_login_required, ]
    model = Subnet
    template_name = 'core/admin_net/subnet_confirm_delete.html'
    success_url = 'subnet_admin:ip_network:list'
    success_text = 'Подсеть успешно удалена.'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, self.success_text)
        return reverse(self.success_url)


class IPAllList(DecoratorChainingMixin, ListView):

    """ Список выданных IP адресов """

    decorators = [admin_and_login_required, ]
    model = IPDelivery
    template_name = 'core/admin_net/ip_list.html'
    context_object_name = 'ip_list'
    paginate_by = 25

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(IPAllList, self).get_context_data(object_list=None, **kwargs)

        context.update({
            'filter_form': FilterForm()
        })
        return context

    def get_queryset(self):

        qs = super(IPAllList, self).get_queryset()

        # Фильтрация `queryset` по форме `FilterForm`
        if self.request.GET.get('subnets'):
            subnet = self.request.GET.get('subnets')
            qs = qs.filter(subnet_id=subnet)

        if self.request.GET.get('users'):
            user = self.request.GET.get('users')
            qs = qs.filter(user_id=user)

        return qs


class IPAllDelete(DecoratorChainingMixin, DeleteView):

    """ Удаление выданного IP адреса """

    decorators = [admin_and_login_required, ]
    pk_url_kwarg = 'ip_net_id'
    model = IPDelivery
    template_name = 'core/admin_net/ip_confirm_delete.html'
    success_url = 'subnet_admin:ip_network:ip_list'
    success_text = 'IP адрес успешно удален.'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, self.success_text)
        return reverse(self.success_url)
