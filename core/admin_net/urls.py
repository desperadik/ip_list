
from django.urls import path, include
from .views import SubnetList, SubnetDelete, SubnetAdd, SubnetEdit, IPAllList, IPAllDelete

app_name = 'ip_network'

ip_network_patterns = (
    [
        path('list/', SubnetList.as_view(), name='list'),
        path('add/', SubnetAdd.as_view(), name='add'),
        path('<int:pk>/edit/', SubnetEdit.as_view(), name='edit'),
        path('<int:pk>/delete/', SubnetDelete.as_view(), name='delete'),

        path('ip_list/', IPAllList.as_view(), name='ip_list'),
        path('<int:ip_net_id>/delete/', IPAllDelete.as_view(), name='ip_delete'),
    ], 'ip_network')


urlpatterns = [
    path('ip-network/', include(ip_network_patterns))
]

