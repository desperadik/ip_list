
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseForbidden


def admin_and_login_required(view_func):
    """
    Декоратор, проверяющий, суперпользователь это или нет.
    """
    @login_required()
    def wrapper(request, *args, **kw):
        if request.user.is_superuser:
            return view_func(request, *args, **kw)
        else:
            return HttpResponseForbidden()
    return wrapper
