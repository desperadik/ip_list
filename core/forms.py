from django import forms

from core.ip_methods import convert_ip_to_network_address, check_ip_in_subnet
from .models import Subnet
from django.contrib.auth import get_user_model

User = get_user_model()


class IPNetworkForm(forms.ModelForm):

    """ Форма создание подсети """

    def __init__(self, *args, **kwargs):

        super(IPNetworkForm, self).__init__(*args, **kwargs)

        if self.instance.pk:
            self.form_title = 'Редактирование подсети'
        else:
            self.form_title = 'Создание подсети'

        for field in self.fields:
            if field == 'ip_net':
                self.fields[field].label = u'Хост или подсеть:'
                self.fields[field].widget.attrs['placeholder'] = u'Укажите хост или подсеть'
                self.fields[field].widget.attrs['data-masked-ip'] = True
            if field == 'mask':
                self.fields[field].label = u'Маска подсети:'

            if field == 'gateway':
                self.fields[field].label = u'Шлюз подсети:'
                self.fields[field].widget.attrs['placeholder'] = u'Укажите шлюз подсети'
                self.fields[field].widget.attrs['data-masked-ip'] = True

    def get_form_title(self):
        return self.form_title

    def clean(self):

        super(IPNetworkForm, self).clean()

        ip_net = self.cleaned_data.get('ip_net', False)
        mask = self.cleaned_data['mask']
        gateway = self.cleaned_data.get('gateway', False)

        if ip_net:
            # Если указали хост, сбрасываем IP до 0.
            self.cleaned_data['ip_net'] = convert_ip_to_network_address(ip_net, mask)

            if gateway:
                # Проверяем, чтобы шлюз был находился в создаваемой подсети.
                if not check_ip_in_subnet(ip_net, mask, gateway):
                    raise forms.ValidationError('Указанный адрес шлюза не принадлежит к данной подсети.')
                # TODO: Нужно добавить проверку, если хотят поменять адрес шлюза, а адрес уже выдан пользователю.
        return self.cleaned_data

    class Meta:
        model = Subnet
        fields = '__all__'


class FilterForm(forms.Form):
    """
    Форма фильтрации сиписка выданных IP
    """

    subnets = forms.ModelChoiceField(queryset=Subnet.objects.all(),
                                     label='Подсеть',
                                     required=False)

    users = forms.ModelChoiceField(queryset=User.objects.all(),
                                   label='Пользователь',
                                   required=False)
