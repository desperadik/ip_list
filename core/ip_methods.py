from ipaddress import IPv4Address, ip_network, ip_address, ip_interface, IPv6Address, IPv4Interface


def get_ip_list_for_subnet(subnet_ip, subnet_mask):
    """ Возвращает лист доступных хостов для данной подсети """
    return list((ip_network('{}/{}'.format(subnet_ip, subnet_mask)).hosts()))


def get_free_ip_from_subnet(subnet_ip, subnet_mask, excluded_ip_list=None):

    """ Возвращает ПЕРВЫЙ свободный IP из данной подсети"""

    if excluded_ip_list is None:
        excluded_ip_list = []
    else:
        # Конвертируем пришедшие адреса в объект класса `IPv4Address` или `IPv4Address`
        excluded_ip_list = convert_ip(excluded_ip_list)

    ip_list_for_subnet = get_ip_list_for_subnet(subnet_ip, subnet_mask)

    ip_list_filtered = [ip for ip in ip_list_for_subnet if ip not in excluded_ip_list]

    # TODO: Добавить обработку исключения, если список отфильтрованных IP пуст.
    ip = ip_list_filtered[0]

    return str(ip)


def convert_ip(ip_list):
    """
    :param ip_list: ['str', 'str', ....]
    :return: converted_ip_list: [`ip_address.object`, `ip_address.object`, ....]
    """

    converted_ip_list = []

    for ip in ip_list:
        converted_ip_list.append(ip_address(ip))
    return converted_ip_list


def convert_ip_to_network_address(ip, mask):
    """
    :param ip: str
    :param mask: str
    :return: str, возвращаем начальный `network_address` для подсети.

    Например, если пришел 192.168.0.23/24, то вернет 192.168.0.0.
    """
    return str(ip_interface('{}/{}'.format(ip, mask)).network.network_address)


def check_ip_in_subnet(subnet_ip, mask, ip):
    """
    Проверяет, что `ip` принадлежит подсети `subnet_ip`/`mask`
    :param subnet_ip:
    :param mask:
    :param ip:
    :return:
    """

    ip_list = get_ip_list_for_subnet(convert_ip_to_network_address(subnet_ip, mask), mask)

    if ip_address(ip) in ip_list:
        return True
    else:
        return False
