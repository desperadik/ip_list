from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import ListView, DeleteView
from core.models import Subnet, IPDelivery


class IPList(ListView):

    """ Список полученных IP адресов для пользователя """

    model = IPDelivery
    template_name = 'core/user_net/ip_list.html'
    context_object_name = 'ip_list'
    paginate_by = 10

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)


class SubnetListAllowed(ListView):

    """ Список подсетей """

    model = Subnet
    template_name = 'core/user_net/subnet_list.html'
    context_object_name = 'subnet_list'
    paginate_by = 10


class IPDelete(DeleteView):

    """ Удаление полученного IP адреса """

    model = IPDelivery
    template_name = 'core/user_net/ip_confirm_delete.html'
    success_url = 'subnet_user:ip_network:list'
    success_text = 'IP адрес успешно удален.'

    def get_success_url(self):
        messages.add_message(self.request, messages.SUCCESS, self.success_text)
        return reverse(self.success_url)

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)


@login_required
def get_ip_from_subnet(request, subnet_pk):

    """
    Получает IP адрес и создает объект `IPDelivery`
    :param request:
    :param subnet_pk: ИД подсети

    """

    try:
        subnet = Subnet.objects.get(pk=subnet_pk)
    except Subnet.DoesNotExist:
        # TODO: Lv.1 Обработать исключение, вслучае не нахождения Subnet.
        pass
    else:
        delivery_ip = subnet.get_ip()

        try:
            # Создаем объект класса `IPDelivery`.
            ip = IPDelivery.objects.create(
                user=request.user,
                delivery_ip=delivery_ip,
                subnet=subnet
            )
            success_text = 'IP адрес "{}" успешно получен.'.format(ip)
        except Exception as error:
            success_text = error

        messages.add_message(request, messages.SUCCESS, success_text)
        return HttpResponseRedirect(reverse('subnet_user:ip_network:list'))
