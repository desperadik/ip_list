from django.urls import path, include
from .views import IPList, IPDelete, SubnetListAllowed, get_ip_from_subnet
from django.contrib.auth.decorators import login_required

app_name = 'ip_network'

ip_network_patterns = (
    [
        path('list/', login_required(IPList.as_view()), name='list'),
        path('<int:subnet_pk>/get/', get_ip_from_subnet, name='get'),
        path('<int:pk>/delete/', login_required(IPDelete.as_view()), name='delete'),

        path('subnet_list/', login_required(SubnetListAllowed.as_view()), name='subnet_list'),
    ], 'ip_network')


urlpatterns = [
    path('ip-network/', include(ip_network_patterns))
]

